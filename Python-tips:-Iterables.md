Notes about how to make the best use of various Python3 iterable objects

Talk given on 20230120

---
# wording and concepts:

- **mutable**: mutable objects can be changed after they have been created (e.g., lists). Immutable obects can only be replaced by new ones, but not changed. [More info](https://medium.com/geekculture/mutability-in-python-39e483fc9f4d)
- **hashable**: Is an object that implements `.__hash__()` to provide a unique ID that does not change in its lifetime. Only hashable objects can be used as **keys** in a dictionary. Inmutable objects are hashable, and **some** mutable objects may be hashable as well. 
- **iterable**: Is an object that implements `.__iter__()`. It can be used in loops. It does not necessarily need to be of a known length or even be finite!
- **sequence**: is an iterable whose items are deterministically ordered (e.g. a tuple or a list is a sequence, but a set is not). 
- **iterator**: An iterator is  an object that provides the `__next__()` method and can be used to iterate **once** over an iterable. All iterators are iterables, but not all iterables are iterators. [More info](https://www.geeksforgeeks.org/python-difference-iterable-iterator/)
- **slicing**: obtaining a portion of a sequence, tipically using the subscript notation (e.g. mylist[2], or `mylist[3:-2:2]`)
- **variable unpacking**: assigning the contents of a sequence to one or more variables (e.g.: `a, b = (1, 2)`)
- **generator**: a function that returns a "generator iterator". It uses yield instead of return. Won't be discussed further in these notes.

See also: [The python glossary](https://docs.python.org/3/glossary.html)


# Iterable types

I'll discuss about the following iterable classes:

- strings / bytes
- tuples / lists
- ranges
- sets / frozensets
- dicts
- ndarrays

The above are the most standard iterable types. All except ndarrays are base types.

The standard library also provides the  [collections](https://docs.python.org/3/library/collections.html) module with some interesting classes, of which I recommend looking at:
- defaultdict
- namedtuple
- Count

See also the [collections.abc](https://docs.python.org/3/library/collections.abc.html) module to understand the class hierarchy of the various iterables, which is **useful for choosing appropriate type hints**.

## strings
They are immutable sequences of UTF characters: `"hello"` or `"I 💜 π-ton"`

Interesting methods:
- `lower()` / `upper()`
- `strip()`
- `replace()`
- `index()`
- `format()`
- `split()`, `rsplit()`
- `startswith()` / `endswith()`
- `join()`
- ... (see https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str)


## bytes
Similar to strings, but limited to ASCII characters: `b"hello"`. 

Typically used for I/O protocols.

(See https://docs.python.org/3/library/stdtypes.html#bytes )

## tuples

They are immutable sequences of arbitrary objects: `(5, "foo", os.path)`, `()` , `7, 1.9`.

They are hashable. Can be used as dict keys.

## lists

They are mutable sequences of arbitrary objects.

Creating lists: 
  - list literal: `[3, "foo", os.path]`
  - concatenation, append, insert and extend
  - list comprehension: `a = [1 / n for n in range(-2,3) if n != 0]`

They provide `.reverse()`, `.index()`, ...

They are **not** hashable.

## ranges

Ranges are immutable sequences of integers, created as `range(stop)` or `range(start, stop[, step])`

Contrary to a tuple or a list, **they do not store all the numbers in memory** (they generate them as needed).

They are **much more efficient than lists in a loop**, but they cannot be sliced and their contents cannot be directly printed!

(See: https://docs.python.org/3/library/stdtypes.html#ranges )

## sets

Sets are mutable collections of distinct hashable objects. They are not ordered (they cannot be sliced!).

The set items are guaranteed to be unique. Duplicates are removed on creation or when using `.add()`.

Creating sets:
  - set literal: `{1, 2, "A"}`
  - from other iterables: `set(mylyst)`
  - from set operations (`union()`, `intersection()`, `difference()`, ...)


(See: https://docs.python.org/3/library/stdtypes.html#set-types-set-frozenset)

## frozensets

Frozensets are like sets but they are immutable and hashable (i.e. frozensets are to sets what tuples are to lists)

## dicts

Dicts are mutable "mappings" (collections that map "key" objects with "value" objects). Keys must be hashable. Values can be any object.

The keys and values of a dict are accessible **as iterators**  with: `.keys()`, `.values()` and `.items()`

Iterating over a dict is equivalent **and preferred** to iterating over its keys. (i.e., do **not** do `for k in d.keys(): ...`)

Creating dicts: 
  - dict literal: `{1:[10], "foo": 20}`
  - dict comprehension: `d = {k: 2**k for k in range(8)}`
  - dict constructor:
    - `dict(a=1, b=2)`
    - `dict([("a", 1), ("b", 2)])`
    - `dict(zip("ab", (1,2)))`

## ndarrays

ndarrays are objects provided by the numpy module. They are **homogeneous** mutable sequences of arbitrary objects (i.e., all the items of a ndarray must be of the same type). 

creating ndarrays: Can be created from an iterable `np.array()`

They are implemented in C, and are highly optimized for numerical operations **as long as the numpy ufuncs or methods are used**. 

They are optimized for memory (e.g., creating a slice or reshaping does not involve copying data in RAM), **but resizing is expensive**.



Creating ndarrays:
  - from other iterables: `np.array([1, 2, 3])`
  - from generic numpy functions:
    - `np.ones()`, `np.zeros()`, `np.random.rand()`, `np.empty()`, ...
  - from range-like functions
    - `np.arange()`, `np.linspace()`, `np.logspace()`
  - from specialized functions:
    - `np.indices()`, ...
  - from mathematical expressions:
    - `np.fromfunction(lambda i, j: i + j, (3, 3))`
  - from operating with other ndarrays:
    - `new_array = my_ndarray * 2`
    - `my_nparray.astype(bool)`


(See https://numpy.org/doc/stable/user/basics.creation.html )

### Element-wise operation with ndarrays

Most of the numpy operations are defined "element-wise":

```python
>>> x = np.arange(5)
>>> x ** 2
array([ 0,  1,  4,  9, 16])
```

This **helps avoiding python loops** which are more expensive than the equivalent (implicit) loops implemented in C by numpy.


Functions that apply to ndarrays element-wise are called **ufuncs** ("universal functions"). A regular python function can be "vectorized" to convert it into an ufunc. See `np.frompyfunc()` and `np.vectorize()`



### Caveats: 

#### views are great, but they can bite you!

```python
>>> a = np.arange(5) * 10
>>> a
array([ 0, 10, 20, 30, 40])
>>> b = a[2:4]
>>> b
array([20, 30])
>>> b[0] = -99
>>> b
array([-99, 30])
>>> a
array([ 0, 10, -99, 30, 40])
```


#### beware of truncation

numpy automatically casts on most operations to the required dtype...

```python
>>> a = np.arange(4)
>>> a
array([0, 1, 2, 3])
>>> a.dtype
dtype('int64')
>>> b = a / 10
>>> b
array([0. , 0.1, 0.2, 0.3])
>>> b.dtype
dtype('float64')
```

**but** not when assigning to a member or a slice:

```python
>>> a = np.arange(4)
>>> a
array([0, 1, 2, 3])
>>> a.dtype
dtype('int64')
>>> a[2] / 10
0.2
>>> a[2] = a[2] / 10
>>> a
array([0, 1, 0, 3])
>>> a.dtype
dtype('int64')
```


# Handling iterables

## The power of slicing

The notation for slicing is: `[start[:stop[:step]]]`

```python

>>> a = "abcdefghi"
>>> a[2]
'c'
>>> a[2:5]
'cde'
>>> a[0:4:2]
'ac'
>>> a[2:]
'cdefghi'
>>> a[-2:]
'hi'
>>> a[::-1]
'ihgfedcba'
>>> a[:]
'abcdefghi'

```

## the power of unpacking

```python
>>> m = [1, "foo", [3, 4]]
>>> a, b, (c, d) = m
>>> a, b, c, d
(1, 'foo', 3, 4)
>>> a, (b1, b2, b3), c = m
>>> a, b1, b2, b3, c
(1, 'f', 'o', 'o', [3, 4])
```

## the power of zipping

`zip()` returns an iterator that intermixes 2 or more sequences.

> Hint: is is useful to visualize zip as in a "coat zip" that intermixes two parts

```python
>>> zip(("abcd", "1234"))
<zip object at 0x7f566cb36f00>
>>> list(zip("abcd", "1234"))
[('a', '1'), ('b', '2'), ('c', '3'), ('d', '4')]
```

And it can be used to "unmix":

```python
>>> mixed = [('a', 1, 'A'), ('b', 2, 'B'), ('c', 3, 'C'), ('d', 4, 'D')]
>>> lower, numbers, upper = zip(*mixed)
>>> lower
('a', 'b', 'c', 'd')
>>> numbers
(1, 2, 3, 4)
>>> upper
('A', 'B', 'C', 'D')
```

# Comparing speed efficiency of different iterable types

## speed for _accessing_ items
```python

In [1]: import numpy as np

In [2]: mylist = list(range(1000))

In [3]: myrange = range(1000)

In [4]: mytuple = tuple(range(1000))

In [5]: myset = set(range(1000))

In [6]: mydict = {i:i for i in range(1000)}

In [7]: myfrozenset = frozenset(range(1000))

In [8]: myndarray = np.array(range(1000))

In [9]: %timeit sum(mytuple)
2.54 µs ± 64.9 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)

In [10]: %timeit sum(mylist)
3.48 µs ± 272 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)

In [11]: %timeit sum(myset)
4.64 µs ± 26.9 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)

In [12]: %timeit sum(myrange)
8.95 µs ± 1.25 µs per loop (mean ± std. dev. of 7 runs, 100,000 loops each)

In [13]: %timeit sum(mydict)
5.06 µs ± 48.2 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)

In [14]: %timeit sum(myfrozenset)
4.7 µs ± 62.5 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)

In [16]: %timeit sum(myndarray)
37.3 µs ± 180 ns per loop (mean ± std. dev. of 7 runs, 10,000 loops each)

In [17]: %timeit myndarray.sum()
1.39 µs ± 9.91 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)

```

> **Lesson learned**: ndarrays can make your code much faster, but you should use numpy functions and methods with them, not built-in python ones!

## Speed looking up for an item in an iterable

Testing if an element is in an iterable is **a lot** more efficient with sets and dicts.

```python
In [18]: a =list(range(1000))

In [19]: random.shuffle(a)

In [20]: mylist=list(a)

In [21]: mytuple=tuple(a)

In [22]: myset=set(a)

In [23]: mydict = {i:-i for i in a}

In [24]: myndarray = np.array(a)

In [25]: a[500]
Out[25]: 913

In [26]: %timeit 913 in mylist
3.19 µs ± 10.2 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)

In [27]: %timeit 913 in mytuple
2.83 µs ± 41.9 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)

In [28]: %timeit 913 in myset
22 ns ± 0.215 ns per loop (mean ± std. dev. of 7 runs, 10,000,000 loops each)

In [29]: %timeit 913 in mydict
24.6 ns ± 0.256 ns per loop (mean ± std. dev. of 7 runs, 10,000,000 loops each)

In [30]: %timeit 913 in myndarray
1.94 µs ± 40.7 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)

```

> **Note**: looking up an item in a dict (or set) is a `O(1)` operation (i.e. its performance is constant **regardless of the size of the dictionary**)


# some tips about loops

## Unnecessary indices in your loops should be avoided

> **Rule of thumb**: if you are iterating over an index to access items of a sequence, your code is almost surely sub-optimal.

### For simple loops over an iterable:

We have a list of cities:
```python
cities = ["Paris", "Rome", "Madrid"]
```
Let's print them:

😖
```python
i = 0
while i < 3:
    print(cities[i])
    i += 1
```

😕
```python
for i in range(len(cities)):
    print(cities[i])
```

🙂
```python
for city in cities:
    print(city)
```

### simultaneous iteration over several iterables

Now we want to match the cities with their countries and print them:

```python
cities = ["Paris", "Rome", "Madrid"]
countries = ["France", "Italy", "Spain"]
```

😖
```python
for i in (range(len(cities)):
    print(cities[i], country[i])
```

😕
```python
for i, city in enumerate(cities):
    print(city, country[i])
```

🙂
```python
for city, country in zip(cities, countries):
    print(city, country)
```


And in some cases it would be better if we had the data in a dict:

```python
cities = {"Paris": "France", "Rome": "Italy", "Madrid": "Spain"}
```

😖
```python
for city in cities.keys():
    print(city, cities[city])
```

🙂
```python
for city, country in cities.items():
    print(city, country)
```

... And note, that if we only wanted to print the keys...

😕
```python
for city in cities.keys():
    print(city)
```

🙂
```python
for city in cities:
    print(city)
```


## numpy element-wise operations can avoid python loops:

The element-wise operation in numpy (aka *broadcasting*) is a very good way to optimize numerical operations over large sequences of numbers:

```python
In [31]: numbers = list(range(1000))

In [32]: %timeit [n / 10 for n in numbers ]
25.5 µs ± 58.6 ns per loop (mean ± std. dev. of 7 runs, 10,000 loops each)

In [33]: numbers = np.arange(1000)

In [34]: %timeit numbers / 10
2 µs ± 7.7 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)

# and if we choose dtypes wisely to avoid castings...
In [35]: numbers = np.arange(1000, dtype=float)

In [36]: %timeit numbers / 10.
1.13 µs ± 25.2 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)
```

Another example:

Find which item of `a=[23, 666, 42, 89, 287, 529]` is closest to x=123:

```python
>>> a = np.array([23, 666, 42, 89, 287, 529])
>>> x = 123
>>> closest = a[np.abs(a - x).argmin()]
>>> closest
89
```

## Some functions and methods may allow to replace explicit loops:

### map

We can use map to apply a function over all items of an iterable. **It returns an iterator**:

```python
map(lambda x: str(x**2), range(1000))
```
... is roughly equivalent to:
```python
[str(x**2) for x in range(1000)]
```

... but **uses less memory**, it is generally a bit faster and the **calculation is delayed until the iterator is used** :

```python
In [37]: %timeit [str(n) for n in range(1000) ]
79.7 µs ± 1.95 µs per loop (mean ± std. dev. of 7 runs, 10,000 loops each)

In [38]: %timeit map(str, a)
80.8 ns ± 2.44 ns per loop (mean ± std. dev. of 7 runs, 10,000,000 loops each)

In [39]: %timeit list(map(str, a))
66.3 µs ± 1.58 µs per loop (mean ± std. dev. of 7 runs, 10,000 loops each)
```

... which means that the **calculation is only done for those elements that are actually accessed!**:

```python
In [40]: def all_same(iterable):
     ...:     """Gets an iterable of 3-character "words" and returns
     ...:     the first item in which all 3 characters are equal
     ...:     """
     ...:     for word in iterable:
     ...:         c0, c1, c2 = word
     ...:         if c0 == c1 == c2:
     ...:             return word
     ...: 

In [41]: %timeit all_same([str(n) for n in range(100,1000)])
78.5 µs ± 2.1 µs per loop (mean ± std. dev. of 7 runs, 10,000 loops each)

In [42]: %timeit all_same(map(str, range(100,1000)))
1.89 µs ± 38.6 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)
```

(note that in the list case, the `str()` function was called 899 times, while in the map case, only it was called only 11 times)

### filter

We can use it to "extract" items from an iterable based on a condition. **It returns an iterator**:

If we want to extract the even numbers from `a = [1, 4, 8, 3, 1, 5, 2]`

```python
filter(lambda x: x % 2 ), a)
```
... is roughly equivalent to:
```python
[ x for x in a if x % 2 ]
```

> **Note**: In my experience, though, filter is usually slower than the list comprehension alternative.

## Filtering in ndarrays using slice masks

A similar result to `filter` can be attained for ndarrays using "masking" slices:

Numpy arrays allow to pass a ndarray of booleans as a mask, and only the elements with `True` in the mask are returned:

```python
>>> a = np.array([3, 9, 5, 6, 1, 4])
>>> mask = np.array((False, True, True, False, False, False))
>>> a[mask]
array([9, 5])
```

And if we combine this with the fact that ndarray operations are broadcasted element-wise, we can perform very convenient filter operations without python loops:

```python
>>> a < 5
array([True, False, False, False, True, True])
>>> a[a < 5]
array([3, 1, 4])
```

See also: `np.where()` and `np.choose()`


## avoid conversions (choose the right iterable and stick to it)


> **Rule of thumb**: Every time you see a casting in your code, examine if it is really needed. 


Try to design your code to minimize iterable conversions. Specially if your iterables are large.

For example:
- if you expect to access the items non-sequentially (e.g. picking 
one or a few selected items at a time), consider using a `dict`.
- if the items are homogeneous and numerical, consider `numpy array`s. Specially if you expect to operate mathematically with the whole iterable.
- if the items are not going to be modified after being created, consider using a `tuple` or a `frozenset`.
- if you are going to test if an object is within the iterable, consider a `dict` or a set or a `frozenset`.
- if the items are only needed during a loop (or if the iterable is only going to be used for a single iteration), consider  `iterators` or `range` objects instead of lists or tuples.


😖
```python
for i in list(range(1000)): ...
```

😖
```python
for i, puc in list(enumerate(pucs)): ...
```

Choosing the data according to its usage is very important, but still sometimes casting is required. In such case, try to minimize it. (e.g. try to avoid castings within loops).


> **Note about type hinting**: In many occasions, for **public** APIs it is interesting to be flexible in the typing (e.g. if you just need that an argument is iterable and finite-length, hint it as a `Sequence`, instead of as a `List`). Apply [duck-typing](https://en.wikipedia.org/wiki/Duck_typing). On the other hand, be as much specific as possible about the return values of your methods. Also, you may be restrictive in typing in the private APIs, to facilitate debugging. 

## Avoid using lists/tuples/ndarrays for looking up values:

Assume we have a one-to-one mapping between `x` and `y`:
```python
x = list(range(1000))
y = list(range(999, -1, -1))
```

We want to find the value of x that corresponds to the value y=123:

😖  (~20 µs with pre-created lists)
```python
for i, n in enumerate(y):
    if n == 123:
        result = x[i]
        break
```

😕 (~6 µs with pre-created lists)
```python
result = x[y.index(123)]
```

🙂  (~20 ns, with pre-created dict)
```python
y_to_x = dict(zip(y, x))
result = y_to_x[123]
```

If we want to account for the data creation time, it is ~15 µs for creating the lists and ~35 µs for creating the dict. This means that using the second implementation could be better than the 3rd **but only if one or two lookups are done**



## Replacing some long if-elif-else chains with a dict 

Consider this code which tells (very subjectively) who you are based on your linux distro of choice:

```python
def people_from_distros(distros):
    people = []
    for distro in distros:
        if distro in ("ubuntu", "mint"):
            people.append("newbie")
        elif distro in ("debian", "archlinux"):
            people.append("hacker")
        elif distro == "kali":
            people.append("cracker")
        elif distro == "tails":
            people.append("paranoid")
        elif distro == "RedHat":
            people.append("corporate sysadmin")
        elif distro == "Suse":
            people.append("german corporate sysadmin")
        elif distro == "popOS":
            people.append("iPronics nerd")
        elif distro in ("Windows", "OSX"):
            people.append("clueless")
        else:
            people.append("???")
    return people
```

This code is `O(N)`  (its performance decreases linearly with the number of choices supported).

It can be transformed into `O(1)` (performance keeps constant regardless of number of choices supported) by using a dictionary:

```python
DISTRO_PREJUDICES = {
    "ubuntu": "newbie",
    "mint": "newbie",
    "debian":"hacker",
    "archlinux": "hacker",
    "kali": "cracker",
    "tails": "paranoid",
    "RedHat": "corporate sysadmin",
    "Suse": "german corporate sysadmin",
    "popOS": "iPronics nerd",
    "Windows":"clueless",
    "OSX": "clueless",
}

def people_from_distros_ng(distros):
    people = []
    for distro in distros:
        people.append(DISTRO_PREJUDICES.get(distro, "???"))
    return people
```

```python
In [43]: %timeit people_from_distros(["debian", "popOS", "Slackware"])
442 ns ± 7.98 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)

In [44]: %timeit people_from_distros_ng(["debian", "popOS", "Slackware"])
229 ns ± 7.56 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)
```
## match-case (Python>=3.10)

Using match-case construct (introduced in python 3.10) can also be a more readable and efficient alternative to long if-else chains:

```python
def people_from_distros_match(distros):
    people = []
    for distro in distros:
        match distro:
            case "ubuntu" | "mint":
                people.append("newbie")
            case "debian" | "archlinux":
                people.append("hacker")
            case "kali":
                people.append("cracker")
            case "tails":
                people.append("paranoid")
            case "RedHat":
                people.append("corporate sysadmin")
            case "Suse":
                people.append("german corporate sysadmin")
            case "popOS":
                people.append("iPronics nerd")
            case "Windows" | "OSX":
                people.append("clueless")
            case _:
                people.append("???")
    return people
```

Executing **in python 3.11**:

```python
In [6]: %timeit people_from_distros(["debian", "popOS", "Slackware"])
314 ns ± 7.04 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)

In [7]: %timeit people_from_distros_ng(["debian", "popOS", "Slackware"])
156 ns ± 1.65 ns per loop (mean ± std. dev. of 7 runs, 10,000,000 loops each)

In [8]: %timeit people_from_distros_match(["debian", "popOS", "Slackware"])
305 ns ± 9.03 ns per loop (mean ± std. dev. of 7 runs, 1,000,000 loops each)
```

See more: https://peps.python.org/pep-0636